# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  results = ''
  #loop through string and check if char is lowercase or not (upcase it)
  str.each_char do |char|
    results += char if char == char.upcase
  end

  results
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  return str[(str.length/2)] if str.length.odd?
  str[(str.length/2)-1, 2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char { |char| count += 1 if VOWELS.include?(char.downcase)}
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  results = ''
  arr.each_with_index do |el, i|
    results += el
    results += separator unless i == arr.length-1
  end

  results

end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  results = []
  i = 0
  while i < str.length
    results << str[i].downcase if i.even?
    results << str[i].upcase if i.odd?
    i += 1
  end

  results.join('')
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  results = []
  words = str.split(' ')
  words.each do |word|
    if word.length >= 5
      results << word.reverse
    else
      results << word
    end
  end

  results.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
    range = (1..n).to_a
    results = []
    range.each do |num|
      if num % 15 == 0
        results << 'fizzbuzz'
      elsif num % 3 == 0
        results << 'fizz'
      elsif num % 5 == 0
        results << 'buzz'
      else
        results << num
      end
    end
    results
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  results = []
  arr.each {|el| results.unshift(el)}
  results
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return true if num == 2
  return false if num <= 1

  i = 2
  while i < num
    return false if num % i == 0
    i += 1
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  i = 1
  while i <= num
    factors << i if num % i == 0
    i += 1
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []
  factors = factors(num)
  factors.select {|factor| prime?(factor)}

end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
    #separate out even from odd
    odds = []
    evens = []
    arr.each do |num|
      odds << num if num.odd?
      evens << num if num.even?
    end
    #compare which one only has one element in it
    return odds[0] if odds.length == 1
    evens[0]

end
